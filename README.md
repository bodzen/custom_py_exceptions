# Custom Py Exceptions

This module contains all Bodzen's custom python exceptions

## Usage
```
>>> from custom_py_exceptions import CUSTOM_EXCEPTION_OR_FUNCTION
```
Un-explicit list of CUSTOM_EXCEPTION_OR_FUNCTION possible values:
* SubclassMissingVariableError
* SubclassMissingMethodError
* SubclassNameError
* error_msg_for_superclass_missing_method
* error_msg_for_superclass_missing_variable

## Installation

### requirements.txt && pip
```
-e git+ssh://git@gitlab.com/bodzen/custom_py_exceptions.git#egg=custom_py_exceptions
```
