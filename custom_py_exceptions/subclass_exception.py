#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    Custom "subclass Exceptions"
"""

from custom_py_exceptions.generic import CustomException


__all__ = ['SubclassMissingVariableError',
           'SubclassMissingMethodError',
           'SubclassNameError']


class SubclassMissingVariableError(CustomException):
    """
        Custom exception for missing instance
        variable in subclass
    """
    def build_error_msg(self, subclass_name, missing_var_name) -> str:
        return '{0} subclass is missing the instance ' \
            'variable {1}"'.format(subclass_name, missing_var_name)


class SubclassMissingMethodError(CustomException):
    """
        Custom exception for missing method
        in subclass
    """
    def build_error_msg(self, subclass_name, method_name) -> str:
        return '{0} subclass is missing the method: ' \
            '"{1}"'.format(subclass_name, method_name)


class SubclassNameError(CustomException):
    """
        Custom exception for invalid
        subclass name
    """
    def build_error_msg(self,
                        subclass_name: str,
                        superclass_name: str) -> str:
        return '{0} subclass is not a class meant to inherit ' \
            'from {1}'.format(subclass_name, superclass_name)
