#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Custom Exception Master class
"""

from custom_py_exceptions.generic.master_custom_exception import \
    CustomException
