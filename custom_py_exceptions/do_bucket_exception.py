#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    Custom exception used with DigitalOcean Spaces
"""

from custom_py_exceptions.generic import \
    CustomException


__all__ = ['DoSpace_BucketDoesNotExist',
           'DoSpace_FilenameToUploadTypeError',
           'DoSpace_InvalidUploadedFilename',
           'DoSpace_FileToUploadDoesNotExist',
           'DoSpace_InvalidBucketVarType',
           'DoSpace_InvalidExpirationTime',
           'DoSpace_InvalidExpirationType']


class DoSpace_BucketDoesNotExist(CustomException):
    """
        Custom exception when bucket does not exist
            - param0: (str) bucket's name
    """
    def build_error_msg(self, bucket_name) -> str:
        return 'The bucket named: "{0}" does not exist.'.format(bucket_name)


class DoSpace_FilenameToUploadTypeError(CustomException):
    """
        Custom exception when the type of the variable
        containing the filename to upload is invalid
            - param0: (type) filename's type
    """
    def build_error_msg(self, var_type) -> str:
        return 'Invalid type for the variable containing ' \
            'the filename to upload: "{0}".'.format(str(var_type))


class DoSpace_InvalidUploadedFilename(CustomException):
    """
        Custom exception when specified filename
        for the remote file is invalid
            - param0: (str) File to upload name
    """
    def build_error_msg(self, filename) -> str:
        return 'Invalid filename for the destination remote ' \
            'file: "{0}"'.format(filename)


class DoSpace_FileToUploadDoesNotExist(CustomException):
    """
        Custom exception when specified file
        to upload does not exist.
            - param0: (str) filename to upload
    """
    def build_error_msg(self, filename) -> str:
        return 'The specified file to upload does ' \
            'not exist: "{0}"'.format(filename)


class DoSpace_InvalidBucketVarType(CustomException):
    """
        Custom exception when the type of the variable
        containing the bucket's name is invalid
            - param0: (type) bucket's type
    """
    def build_error_msg(self, bucket_type) -> str:
        return 'Invalid type for the variable containing ' \
            'the bucket\'s name: "{0}".'.format(str(bucket_type))


class DoSpace_InvalidExpirationTime(CustomException):
    """
        Custom exception when the specified expiration
        time value is invalid
            - param0: (int) expiration time
    """
    def build_error_msg(self, expiration) -> str:
        return 'The expiration time is not valid:' \
            '"{0}"'.format(expiration)


class DoSpace_InvalidExpirationType(CustomException):
    """
        Custom exception when the specified expiration
        time value is invalid
            - param0: (type) expiration type
   """
    def build_error_msg(self, expiration: type) -> str:
        return 'The expiration type is not valid:' \
            '"{0}"'.format(expiration)
