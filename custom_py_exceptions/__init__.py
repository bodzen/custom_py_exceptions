#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This module provides Bodzen's custom python exceptions
"""

__version__ = '1.4'

from custom_py_exceptions.subclass_exception import \
    SubclassMissingVariableError, \
    SubclassMissingMethodError, \
    SubclassNameError

from custom_py_exceptions.superclass_errors import \
    error_msg_for_superclass_missing_method, \
    error_msg_for_superclass_missing_variable

from custom_py_exceptions.do_bucket_exception import \
    DoSpace_BucketDoesNotExist, \
    DoSpace_FilenameToUploadTypeError, \
    DoSpace_InvalidUploadedFilename, \
    DoSpace_FileToUploadDoesNotExist, \
    DoSpace_InvalidBucketVarType, \
    DoSpace_InvalidExpirationTime, \
    DoSpace_InvalidExpirationType
