#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Output functions to use when superclass
    validation failed
"""


__all__ = ['error_msg_for_superclass_missing_method',
           'error_msg_for_superclass_missing_variable']


def error_msg_for_superclass_missing_method(superclass_name: str,
                                            method_name: str) -> str:
    return 'Superclass {0} lacks "{1}" method'.format(superclass_name,
                                                      method_name)


def error_msg_for_superclass_missing_variable(superclass_name: str,
                                              var_name: str) -> str:
    return 'Superclass {0} lacks "{1}" instance variable'.format(superclass_name,
                                                                 var_name)
