#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore
import custom_py_exceptions as my_package

setup(
    name='custom_py_exceptions',
    version=my_package.__version__,
    author='dh4rm4',
    description='Provides Bodzen\'s custom python exceptions',
    install_requires=[],
    classifiers=[
        "Programming Language :: Python 3.7.2",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
