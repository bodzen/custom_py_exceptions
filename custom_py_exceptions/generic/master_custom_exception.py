#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Custom Exception Master class
"""


class CustomException(Exception):
    def __init__(self, *args: tuple) -> None:
        error_msg = self.build_error_msg(*args)
        super().__init__(error_msg)
